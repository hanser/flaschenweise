# flaschenweise

`flaschenweise` is a simple calculator to support the people behind a bar not having a cash register to calculate the amount due.

It is not registering any transaction but is really just meant to counter miscalculations done by the cashier.

It is best used on a tablet, smartphone or any other touchscreen with a horizontal resolution.

All calculations are done in the browser of the client. Nothing is tracked by any means.

Just try the [demo](https://hanser.codeberg.page/flaschenweise/).

![Screenshot](https://codeberg.org/hanser/flaschenweise/raw/branch/master/assets/screenshot.png)

## Features

### implemented

* Deposits on the beverage containers (Pfand)
* Deposit tokens (Pfandmarken)
* Configuration via json file
* Convenient direct link for android

### missing (PR welcome)

* translation
* ...

## Usage

On Android try adding a the website to the startup screen -- opening `flaschenweise` via this link will open it in fullscreen mode.

To enter an order, just click on the according beverage button. The beverage will be added to the shopping cart *including* potential deposit and a potential deposit token.
If in return the customer brings back beverage containers realted to a deposit, press the "deposit returned" button. Click on the buttons potentially multiple times to match the order.

## Installation

### with an existing http-Server

Just copy the contents of this repository (e.g. from the [ZIP-file](https://codeberg.org/hanser/flaschenweise/archive/master.zip) to your http server and access it with a browser. There are no requirements such as PHP or a Database.

You can also clone the git Repo:

```
git clone https://codeberg.org/hanser/flaschenweise.git
cd flaschenweise
```

#### jQuery
To download the **required** jQuery library, call the following from within the main directory.

```
wget -O jquery.min.js "https://code.jquery.com/jquery-3.7.1.min.js"
```

#### B612 Font
To download the *optional* but very readable and thus recommended [B612 font](https://github.com/polarsys/b612) *"to be used on aircraft cockpit screens"*, call the following from within the main directory.

```
mkdir fonts
wget -O fonts/B612-Regular.ttf "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612-Regular.ttf"
wget -O fonts/B612-Bold.ttf "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612-Bold.ttf"
```

### Test-Setup

You can also test run it by installing python and calling the following line inside the directory.

```
python -m http.server
```

For python2 use:

```
python -m SimpleHTTPServer 8000
```

Afterwards open `flaschenweise` here [http://localhost:8000](http://localhost:8000).

## Configuration

To configure the beverages, prices, deposits and the usage of deposit tokens just edit [products.json](https://codeberg.org/hanser/flaschenweise/src/branch/master/products.json) and refresh the website.
The default configuration should be self-explanatory.

## License

[MIT License](https://codeberg.org/hanser/flaschenweise/raw/branch/master/LICENSE)
