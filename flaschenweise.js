var productData;
var productCount;
var depositCount;
var tokens;
var decimals;
var currency = "";
var calculator = false;
var amountGiven = 0;
var weGetMoney = 0;
var weGetTokens = 0;

// load configuration
$.getJSON("products.json").done(function( data )
{
    productData = data;
    productCount = productData.products.length;
    depositCount = productData.deposits.length;
    tokens = data.tokens;
    decimals = data.decimals;
    if(typeof data.currency != 'undefined')
    {
        currency = " "+data.currency;
    }
    if(typeof data.calculator != 'undefined')
    {
        calculator = data.calculator;
    }
    initial();
    calculatorUpdate();
});

const Product = ({ svg, name, price }) => `
<table>
    <tr style="height:50%">
        <td>
            <div class="newItemDiv ${svg}"></div>
        </td>
    </tr>
    <tr style="height:25%">
        <td class="alignCenter">${name}</td>
    </tr>
    <tr style="height:25%">
        <td class="alignCenter">${price}</td>
    </tr>
</table>
`;

// initialize GUI
function initial()
{
    // add product buttons according to configuration
    $.each( productData.products, function( i, item ) {
        productData.products[i].count = 0;
        $("#productPlus").append($("<td>").attr("width",1/productCount*100+"%").click(function () {productData.products[i].count = productData.products[i].count+1; calculatorReset(); update();}).html([{ svg: "svg_"+item.id, name: item.name, price: parseFloat(item.price).toFixed(decimals)+currency }].map(Product).join('')))

        $(".svg_"+item.id).load("images/"+item.icon)

        // add product to cart table
        $("<tr>").appendTo("#cartProductTable").append($("<td>").attr("id",item.id+"Count").attr("class","cartAmount")).append($("<td>").attr("class","cartDescription").text(item.name));
    });

    if(depositCount > 1)
    {
        // add deposit headline (each deposit is its own entry)
        $("<tr>").appendTo("#cartProductTable").append($("<td>").attr("class","headLineSoft cartAmount")).append($("<td>").attr("class","headLineSoft cartDescription").text("Pfand zurück"));
    }

    // add reset button
    $("<td>").attr("class","noPadding alignCenter").text("Reset").appendTo("#depositPlus").click(function(){reset();});

    // add space between reset button and deposit buttons
    if(productCount - depositCount - 1 > 0)
    {
        $("<th>").attr("colspan",productCount - depositCount - 1).appendTo("#depositPlus");
    }

    // add deposit buttons according to configuration
    $.each( productData.deposits, function( i, item ) {
        productData.deposits[i].count = 0;
        $.each( productData.products, function( j, productItem ) {
            if(productItem.deposit == item.id)
            {
                productData.products[j].deposit = productData.deposits[i];
            }
        });
        $("<td>").attr("class","noPadding").appendTo("#depositPlus").click(function () {productData.deposits[i].count = productData.deposits[i].count+1; calculatorReset(); update();}).append($("<table>").append($("<tr>").append($("<td>").attr("class","alignCenter").text(item.name))).append($("<tr>").append($("<td>").attr("class","alignCenter").text(parseFloat(item.price).toFixed(decimals)+currency))))

        // add deposit line to cart
        if(depositCount > 1)
        {
            $("<tr>").appendTo("#cartProductTable").append($("<td>").attr("id",item.id+"Count").attr("class","cartAmount")).append($("<td>").attr("class","cartDescription").text(parseFloat(item.price).toFixed(decimals)+currency));
        }
        else
        {
            $("<tr>").appendTo("#cartProductTable").append($("<td>").attr("class","headLineSoft cartAmount").attr("id",item.id+"Count")).append($("<td>").attr("class","headLineSoft cartDescription").text("Pfand zurück"));
        }
    });

    // add result area
    if(tokens)
    {
        // we get
        $("#cartResultTableWeGetHead").append($("<th>").attr("colspan","2").attr("class","headLine").text("Wir bekommen"));
        $("#cartResultTableWeGetBody").append($("<td>").attr("class","result").append($("<font>").attr("id","weGetMoney")));
        $("#cartResultTableWeGetBody").append($("<td>").attr("class","result").append($("<font>").attr("id","weGetTokens")));

        // they get
        $("#cartResultTableTheyGetHead").append($("<th>").attr("colspan","2").attr("class","headLine").text("Gast bekommt"))
        $("#cartResultTableTheyGetBody").append($("<td>").attr("class","result").append($("<font>").attr("id","theyGetMoney")));
        $("#cartResultTableTheyGetBody").append($("<td>").attr("class","result").append($("<font>").attr("id","theyGetTokens")));
    }
    else
    {
        // we get
        $("#cartResultTableWeGetHead").append($("<td>").attr("class","headLine").text("Wir bekommen"));
        $("#cartResultTableWeGetBody").append($("<td>").attr("class","result").append($("<font>").attr("id","weGetMoney")));

        // they get
        $("#cartResultTableTheyGetHead").append($("<td>").attr("class","headLine").text("Gast bekommt"))
        $("#cartResultTableTheyGetBody").append($("<td>").attr("class","result").append($("<font>").attr("id","theyGetMoney")));
    }
    amountGiven = 0;
}

// udpate GUI after user interaction
function update()
{
    // reset result – will be calculated now
    weGetMoney = 0;
    weGetTokens = 0;

    // calculate products
    $.each( productData.products, function( i, item ) {
        weGetMoney = weGetMoney + item.count*item.price + item.count*item.deposit.price;
        if(item.deposit.token)
        {
            weGetTokens = weGetTokens-item.count;
        }
        if(item.count > 0)
        {
            $("#"+item.id+"Count").text(item.count);
        }
        else
        {
            $("#"+item.id+"Count").text("");
        }
    });

    // calculate deposits
    $.each( productData.deposits, function( i, item ) {
        weGetMoney = weGetMoney - item.count*item.price;
        if(item.token)
        {
            weGetTokens = weGetTokens+item.count;
        }
        if(item.count > 0)
        {
            $("#"+item.id+"Count").text(item.count);
        }
        else
        {
            $("#"+item.id+"Count").text("");
        }
    });

    // display result money
    if(calculator && amountGiven > 0)
    {
        amountGivenNormalized = amountGiven/10**decimals;
        $("#weGetMoney").text(parseFloat(amountGivenNormalized).toFixed(decimals)+currency);
        $("#weGetMoney").addClass("disabled");
        $("#theyGetMoney").text(parseFloat(amountGivenNormalized-weGetMoney).toFixed(decimals)+currency);
    }
    else
    {
        $("#weGetMoney").removeClass("disabled");
        if(weGetMoney > 0)
        {
            $("#weGetMoney").text(parseFloat(weGetMoney).toFixed(decimals)+currency);
            $("#theyGetMoney").text("");
        }
        else if(weGetMoney < 0)
        {
            $("#weGetMoney").text("");
            $("#theyGetMoney").text(parseFloat(-weGetMoney).toFixed(decimals)+currency);
        }
        else
        {
            $("#weGetMoney").text("");
            $("#theyGetMoney").text("");
        }
    }

    // display result tokens
    if(weGetTokens > 0)
    {
        $("#weGetTokens").text(weGetTokens+" ").append($("<div>").attr( "class", "tokenDiv").load("images/p.svg"));
        $("#theyGetTokens").text("");
    }
    else if(weGetTokens < 0)
    {
        $("#weGetTokens").text("");
        $("#theyGetTokens").text(-weGetTokens+" ").append($("<div>").attr( "class", "tokenDiv").load("images/p.svg"));
    }
    else
    {
        $("#weGetTokens").text("");
        $("#theyGetTokens").text("");
    }
}

// reset cart
function reset()
{
    $.each( productData.products, function( i, item ) {
        productData.products[i].count = 0;
    });
    $.each( productData.deposits, function( i, item ) {
        productData.deposits[i].count = 0;
    });
    calculatorReset();
    update();
}

function calculatorUpdate()
{
    $("#calculatorAmountGiven").text(parseFloat(amountGiven/100.0).toFixed(decimals)+currency);
    if(weGetMoney > 0)
    {
        $("#calculatorWeGetMoney").text("Soll: "+parseFloat(weGetMoney).toFixed(decimals)+currency);
    }
    else
    {
        $("#calculatorWeGetMoney").text("");
    }
    if(amountGiven/100.0 >= weGetMoney)
    {
        $("#buttonLeft").addClass("buttonLeft");
        $("#buttonLeft").removeClass("buttonSingle");
        $("#buttonRight").addClass("buttonRight");
        $("#buttonRight").removeClass("buttonInvisible");
    }
    else
    {
        $("#buttonLeft").removeClass("buttonLeft");
        $("#buttonLeft").addClass("buttonSingle");
        $("#buttonRight").removeClass("buttonRight");
        $("#buttonRight").addClass("buttonInvisible");
    }
}

function calculatorOn() {
    if(weGetMoney != 0)
    {
        document.getElementById("calculator").style.display = "block";
        calculatorReset();
        calculatorUpdate();
    }
}

function calculatorAbort() {
    document.getElementById("calculator").style.display = "none";
    calculatorReset();
    update();
}

function calculatorConfirm() {
    if(amountGiven/100.0 >= weGetMoney)
    {
        document.getElementById("calculator").style.display = "none";
        update();
    }
}

function calculatorReset()
{
    amountGiven = 0;
}
